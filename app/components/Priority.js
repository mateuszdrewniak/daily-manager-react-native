import React from 'react'
import { Pressable, StyleSheet } from 'react-native'
import colors from '../styles/colors'

import { FontAwesome5 } from '@expo/vector-icons'

const priorities = [
  {
    icon: 'minus',
    color: colors.green,
  },
  {
    icon: 'angle-up',
    color: colors.lightSecondary,
  },
  {
    icon: 'angle-double-up',
    color: colors.warning,
  },
]

function Priority(props) {
  let priority = priorities[props.priority]

  return (
    <FontAwesome5 style={[styles.priority, props.style]} name={priority.icon} color={priority.color} size={props.size} />
  )
}

const styles = StyleSheet.create({
  priority: {

  },
})

export default Priority
