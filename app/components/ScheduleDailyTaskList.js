import React, { Component } from 'react'
import { ScrollView, View, StyleSheet, ToastAndroid } from 'react-native'
import uuid from 'react-native-uuid'

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import Card from './Card'
import AppAddButton from './AppAddButton'
import TaskCard from './TaskCard'
import AppText from './AppText'
import AppCloseButton from './AppCloseButton'
import NewTaskCard from './NewTaskCard'

const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

class ScheduleDailyTaskList extends Component {
  constructor(props) {
    super(props)

    this.day = days[props.day || 0]
    this.tasks = props.tasks || []

    this.state = {
      addingDailyTask: props.addingDailyTask,
    }

    this.onAddDailyTaskPress = this.onAddDailyTaskPress.bind(this)
    this.onQuitAddingDailyTaskPress = this.onQuitAddingDailyTaskPress.bind(this)
  }

  render() {
    return (
      <Card style={[mainStyles.fullWidthCard, styles.taskListCard]}>
        <View style={styles.taskListCardTop}>
          <View style={styles.priorityContainer}>
            <AppText style={styles.dayText}>{this.day}</AppText>
          </View>
          { 
            this.state.addingDailyTask ? (
              <AppCloseButton onPress={this.onQuitAddingDailyTaskPress}/>
            ) : (
              <AppAddButton onPress={this.onAddDailyTaskPress}/>
            )
          }
        </View>
        <ScrollView>
          {
            this.state.addingDailyTask ? (
              <NewTaskCard></NewTaskCard>
            ) : null
          }
          {
            this.tasks.map((task) => (
              <TaskCard
                key={uuid.v4()}
                {...task}
                task={task}
                onLongPress={this.props.onLongDailyTaskPress}
                onRemoveTaskPress={(task) => this.props.onRemoveDailyTaskPress(task, this.props.day)}
              />
            ))
          }
        </ScrollView>
      </Card>
    )
  }

  onAddDailyTaskPress() {
    this.setState({ addingDailyTask: true })
    ToastAndroid.show('Choose one of the tasks above', ToastAndroid.SHORT)
    if(this.props.onAddDailyTaskPress) this.props.onAddDailyTaskPress()
  }

  onQuitAddingDailyTaskPress() {
    this.setState({ addingDailyTask: false })
    if(this.props.onQuitAddingDailyTaskPress) this.props.onQuitAddingDailyTaskPress()
  }
}

const styles = StyleSheet.create({
  taskListCard: {
    backgroundColor: colors.group,
  },
  taskListCardTop: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: -5,
  },
  dayText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  priority: {
    marginRight: 3,
  },
  priorityContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
})

export default ScheduleDailyTaskList
