import React from 'react'
import { View, StyleSheet } from 'react-native'
import RadioButtonRN from 'radio-buttons-react-native'

import AppText from './AppText'
import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'

function AppRadioInput(props) {
  return (
    <View style={mainStyles.inputGroup}>
      <AppText style={mainStyles.inputLabel}>{props.label}</AppText>
      <RadioButtonRN
        style={styles.radioContainer}
        boxStyle={styles.radioElement}
        textStyle={styles.radioElementText}
        activeColor={colors.secondary}
        data={props.data}
        circleSize={2}
        initial={props.initial}
        selectedBtn={props.onChange}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  radioContainer: {
    flexDirection: 'row',
    marginTop: -5,
  },
  radioElement: {
    flex: 1,
    paddingVertical: 10,
    marginHorizontal: 2,
    backgroundColor: colors.light,
    elevation: 3,
  },
  radioElementText: {
    color: colors.black,
  },
})

export default AppRadioInput
