import React, { Component } from 'react'
import { View, StyleSheet, Pressable } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker'
import DateAndTime from 'date-and-time'

import AppText from './AppText'

import mainStyles from '../styles/mainStyles'

class AppTimePicker extends Component {
  constructor(props) {
    super(props)

    this.label = this.props.label || 'Time - Duration'
    this.state = {
      show: false,
      time: props.time || '00:30',
      dateTime: new Date(1623105030275),
    }
    this.onChange = this.onChange.bind(this)
  }

  render() {
    return (
      <View style={mainStyles.inputGroup}>
        { this.state.show && (
          <DateTimePicker
            value={this.state.dateTime}
            mode='time'
            is24Hour
            display="default"
            onChange={this.onChange}
          />
        )}
        <AppText style={mainStyles.inputLabel}>{this.label}</AppText>
        <Pressable onPress={() => this.setState({ show: true })} style={[mainStyles.textInput, styles.timeText]}>
          <AppText>{this.state.time}</AppText>
        </Pressable>
      </View>
    )
  }

  onChange(event) {
    if(event.type != 'set') {
      this.setState({ show: false })
      return
    }

    let time = DateAndTime.format(event.nativeEvent.timestamp, 'HH:mm')
    if(this.props.onChange) this.props.onChange(time)
    this.setState({ show: false, dateTime: event.nativeEvent.timestamp, time: time })
  }
}

const styles = StyleSheet.create({
  timeText: {
    paddingVertical: 8,
  },
})

export default AppTimePicker
