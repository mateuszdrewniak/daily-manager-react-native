import React from 'react'
import { View, StyleSheet } from 'react-native'
import colors from '../styles/colors'

import Card from './Card'
import Priority from './Priority'
import AppButton from './AppButton'
import TimeHelper from '../services/TimeHelper'
import AppText from './AppText'

function FullTaskCard(props) {
  let spentTimeStyle
  let percentageBarStyle = styles.percentageBarStyle
  let percent = TimeHelper.percentage(props.spent_time, props.time) || 0
  let percentageBarColor = styles.percentageBarColor
  let buttonText = 'START'
  let buttonStyle = null
  let outline = null
  
  if(props.done) {
    spentTimeStyle = styles.doneSpentTime
    percentageBarStyle = null
    outline = true
  }

  if(props.current) {
    spentTimeStyle = styles.spentTimeCurrent
    percentageBarColor = styles.percentageBarColorCurrent
    buttonText = 'STOP'
    buttonStyle = styles.stopButton
    outline = false
  }

  return (
    <Card style={styles.card}>
      <View style={[percentageBarStyle, percentageBarColor, { width: `${percent}%` }]}/>
      <View style={styles.taskCardContainer}>
        <View style={styles.leftCardRow}>
          <AppText>{truncate(props.name, 20)}</AppText>
          <View style={styles.timeContainer}>
            <AppText style={[styles.underline, spentTimeStyle]}>{props.spent_time}</AppText>
            <AppText>{props.time}</AppText>
          </View>
        </View>
        <View style={styles.middleCardRow}>
          <Priority priority={props.priority} size={32}/>
        </View>
        <View style={styles.rightCardRow}>
          <AppButton parent={this} onPress={props.onPress} outline={outline} style={buttonStyle} label={buttonText}/>
        </View>
      </View>
    </Card>
  )
}

function truncate(str, limit) {
  return (str.length < limit) ? str : str.substring(0, limit).replace(/.{3}$/gi, '...')
}

const styles = StyleSheet.create({
  card: {
    paddingHorizontal: 0,
  },
  taskCardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  stopButton: {
    backgroundColor: colors.warning,
  },
  underline: {
    borderStyle: 'solid',
    borderBottomWidth: 2,
    borderColor: colors.black,
  },
  timeContainer: {
    alignSelf: 'flex-start'
  },
  doneSpentTime: {
    color: colors.green,
  },
  percentageBarStyle: {
    borderWidth: 2,
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2,
    borderColor: colors.main,
    marginTop: -15,
    marginBottom: 10,
  },
  percentageBarColor: {
    borderColor: colors.main,
  },
  percentageBarColorCurrent: {
    borderColor: colors.secondary,
  },
  spentTimeCurrent: {
    color: colors.secondary,
  },
})

export default FullTaskCard
