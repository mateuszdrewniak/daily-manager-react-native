import React from 'react'
import { StyleSheet } from 'react-native'

import Card from './Card'
import AppText from './AppText'
import colors from '../styles/colors'

function NewTaskCard(props) {
  return (
    <Card style={[styles.taskCard, props.style]}>
      <AppText style={styles.newTaskText}>Press on a task to link it</AppText>
    </Card>
  )
}

const styles = StyleSheet.create({
  taskCard: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 26,
    alignItems: 'center',
    backgroundColor: colors.group,
    borderColor: colors.green,
    borderWidth: 1,
  },
  newTaskText: {
    fontSize: 18,
    color: colors.green
  },
})

export default NewTaskCard
