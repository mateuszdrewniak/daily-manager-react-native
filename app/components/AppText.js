import React from 'react'
import { Text, StyleSheet } from 'react-native'
import colors from '../styles/colors'

function AppText(props) {
  return (
    <Text {...props} style={[styles.text, props.style]}/>
  )
}

const styles = StyleSheet.create({
  text: {
    color: colors.black,
    fontSize: 14,
  },
})

export default AppText
