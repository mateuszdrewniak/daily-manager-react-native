import React from 'react'
import { TextInput, View } from 'react-native'

import mainStyles from '../styles/mainStyles'
import AppText from './AppText'

function AppTextInput(props) {
  return (
    <View style={mainStyles.inputGroup}>
      <AppText style={mainStyles.inputLabel}>{props.label}</AppText>
      <TextInput
        style={mainStyles.textInput}
        {...props}
      />
    </View>
  )
}

export default AppTextInput
