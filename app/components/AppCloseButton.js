import React from 'react'
import { StyleSheet } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import colors from '../styles/colors'

import AppButtonWithoutText from './AppButtonWithoutText'

function AppCloseButton(props) {
  return (
    <AppButtonWithoutText style={styles.closeModalButton} onPress={props.onPress}>
      <FontAwesome5 name="times" color={colors.light} size={22} />
    </AppButtonWithoutText>
  )
}

const styles = StyleSheet.create({
  closeModalButton: {
    paddingVertical: 5,
    backgroundColor: colors.red,
  },
})

export default AppCloseButton
