import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'

import PressableCard from './PressableCard'
import Priority from './Priority'
import AppText from './AppText'
import colors from '../styles/colors'
import AppRemoveButton from './AppRemoveButton'

class TaskCard extends Component {
  constructor(props) {
    super(props)

    if(props.addingDailyTask && props.ableToAddToDay) {
      this.addingDailyTaskStyles = styles.ableToAddToDay
    } else if(props.addingDailyTask) {
      this.addingDailyTaskStyles = styles.unableToAddToDay
    }

    this.state = {
      actionsActive: false
    }
  }
  
  render() {
    let taskCard = this
    return (
      <PressableCard onPress={this.props.onPress} onLongPress={() => taskCard.props.onLongPress ? taskCard.props.onLongPress(taskCard.props.task, taskCard) : null} style={[styles.taskCard, this.addingDailyTaskStyles, this.props.style]}>
        <View>
          <AppText style={styles.taskText}>{this.props.name}</AppText>
          <AppText style={styles.taskText}>{this.props.time}</AppText>
        </View>
        {
          this.state.actionsActive ? (
            <View style={styles.actionButtonsContainer}>
              <AppRemoveButton onPress={() => this.props.onRemoveTaskPress ? this.props.onRemoveTaskPress(this.props.task) : null}/>
            </View>
          ) : (
            <Priority size={30} priority={this.props.priority || 0}/>
          )
        }
      </PressableCard>
    )
  }
}

const styles = StyleSheet.create({
  taskCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 20,
  },
  taskText: {
    paddingVertical: 1,
    fontSize: 16,
  },
  ableToAddToDay: {
    borderColor: colors.secondary,
    borderWidth: 1,
    backgroundColor: colors.lightGray,
  },
  unableToAddToDay: {
    borderColor: colors.red,
    borderWidth: 1,
    backgroundColor: colors.lightGray,
  },
  actionButtonsContainer: {
    flexDirection: 'row',
  },
})

export default TaskCard
