import React from 'react'
import { ScrollView, StyleSheet, Dimensions } from 'react-native'
import {
  BarChart,
} from "react-native-chart-kit"

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import AppText from './AppText'
import Card from './Card'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

function AppBarChart(props) {
  let color = props.color || colors.warning
  const chartConfig = {
    backgroundGradientFrom: colors.light,
    backgroundGradientTo: colors.light,
    color: (opacity = 1) => `rgba(90, 116, 128, ${opacity})`,
    fillShadowGradient: color,
    fillShadowGradientOpacity: .7,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
  }

  return (
    <Card style={[mainStyles.fullWidthCard, styles.chartCard]}>
      <AppText style={styles.cardTitleText}>{props.title}</AppText>
      <ScrollView horizontal>
        <BarChart
          style={styles.barChart}
          data={props.data}
          width={props.width || windowWidth * 1.2}
          height={props.height || windowHeight / 3}
          chartConfig={chartConfig}
          verticalLabelRotation={props.verticalLabelRotation || 20}
        />
      </ScrollView>
    </Card>
  )
}

const styles = StyleSheet.create({
  cardTitleText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
})

export default AppBarChart
