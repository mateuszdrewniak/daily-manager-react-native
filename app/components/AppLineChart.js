import React from 'react'
import { ScrollView, StyleSheet, Dimensions } from 'react-native'
import {
  LineChart,
} from "react-native-chart-kit"

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import AppText from './AppText'
import Card from './Card'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const seriesColors = ['34, 171, 101', '66, 153, 225', '241, 97, 53']

function AppLineChart(props) {
  let color = props.color || colors.gray
  const chartConfig = {
    backgroundGradientFrom: colors.light,
    backgroundGradientTo: colors.light,
    color: (opacity = 1) => `rgba(90, 116, 128, ${opacity})`,
    fillShadowGradient: color,
    fillShadowGradientOpacity: .5,
    strokeWidth: 5, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: true,
  }

  let data = props.data
  data.datasets[0].color = (opacity) => `rgba(${seriesColors[0]}, ${opacity * 3})`
  if(data.datasets[1]) data.datasets[1].color = (opacity) => `rgba(${seriesColors[1]}, ${opacity * 3})`
  if(data.datasets[2]) data.datasets[2].color = (opacity) => `rgba(${seriesColors[2]}, ${opacity * 3})`

  return (
    <Card style={[mainStyles.fullWidthCard, styles.chartCard]}>
      <AppText style={styles.cardTitleText}>{props.title}</AppText>
      <ScrollView horizontal>
        <LineChart
          style={styles.barChart}
          data={data}
          width={props.width || windowWidth * 1.2}
          height={props.height || windowHeight / 3}
          chartConfig={chartConfig}
          verticalLabelRotation={props.verticalLabelRotation != null ? props.verticalLabelRotation : 20}
          bezier
        />
      </ScrollView>
    </Card>
  )
}

const styles = StyleSheet.create({
  cardTitleText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
})

export default AppLineChart
