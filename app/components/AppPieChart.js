import React from 'react'
import { ScrollView, StyleSheet, Dimensions } from 'react-native'
import {
  PieChart,
} from "react-native-chart-kit"

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import AppText from './AppText'
import Card from './Card'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const seriesColors = ['34, 171, 101', '66, 153, 225', '241, 97, 53']

function AppPieChart(props) {
  let color = props.color || colors.gray
  const chartConfig = {
    backgroundGradientFrom: colors.light,
    backgroundGradientTo: colors.light,
    color: (opacity = 1) => `rgba(90, 116, 128, ${opacity})`,
    fillShadowGradient: colors.red,
    fillShadowGradientOpacity: .5,
    strokeWidth: 5,
    barPercentage: 0.5,
  }

  let data = props.data
  for(let dataset of data) {
    dataset.legendFontColor = colors.black
    dataset.legendFontSize = 14
  }

  
  return (
    <Card style={[mainStyles.fullWidthCard, styles.chartCard]}>
      <AppText style={styles.cardTitleText}>{props.title}</AppText>
      <PieChart
        data={props.data}
        width={windowWidth - 50}
        height={220}
        chartConfig={chartConfig}
        accessor={"count"}
        backgroundColor={"transparent"}
        paddingLeft={"15"}
        // absolute
      />
    </Card>
  )
}

const styles = StyleSheet.create({
  cardTitleText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
})

export default AppPieChart
