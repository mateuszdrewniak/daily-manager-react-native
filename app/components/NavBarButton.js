import React from 'react'
import { Pressable, StyleSheet } from 'react-native'
import colors from '../styles/colors'

function NavBarButton(props) {
  let rippleConfig = { color: colors.lightMain, ...props.android_ripple }

  return (
    <Pressable {...props} android_ripple={rippleConfig}/>
  )
}

export default NavBarButton
