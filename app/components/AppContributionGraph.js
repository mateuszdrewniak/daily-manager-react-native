import React, { Component } from 'react'
import { StyleSheet, Dimensions, View } from 'react-native'
import {
  ContributionGraph,
} from "react-native-chart-kit"

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import AppText from './AppText'
import Card from './Card'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

class AppContributionGraph extends Component {
  constructor(props) {
    super(props)

    this.chartConfig = {
      backgroundGradientFrom: colors.light,
      backgroundGradientTo: colors.light,
      color: (opacity = 1) => `rgba(90, 116, 128, ${opacity})`,
      fillShadowGradient: colors.red,
      fillShadowGradientOpacity: .5,
      strokeWidth: 5,
      barPercentage: 0.5,
    }

    let today = props.data[props.data.length - 1]

    this.state = {
      selectedDate: today.date,
      selectedHours: Math.floor(today.count / 60),
      selectedMinutes: Math.floor(today.count % 60),
    }

    this.onDayPress = this.onDayPress.bind(this)
  }
  
  render() {
    return (
      <Card style={[mainStyles.fullWidthCard, styles.chartCard]}>
        <AppText style={styles.cardTitleText}>{this.props.title}</AppText>
        <View style={styles.selectedDayDetailsWrapper}>
          <AppText style={styles.selectedDayHeading}>Date:</AppText>
          <AppText style={styles.selectedDayData}>{this.state.selectedDate}</AppText>
          <AppText style={styles.selectedDayHeading}>Hours:</AppText>
          <AppText style={styles.selectedDayData}>{this.state.selectedHours}</AppText>
          <AppText style={styles.selectedDayHeading}>Minutes:</AppText>
          <AppText style={styles.selectedDayData}>{this.state.selectedMinutes}</AppText>
        </View>
        <ContributionGraph
          values={this.props.data}
          endDate={this.props.endDate}
          numDays={this.props.numDays}
          width={windowWidth - 50}
          height={windowHeight / 3}
          chartConfig={this.chartConfig}
          onDayPress={this.onDayPress}
        />
      </Card>
    )
  }

  onDayPress(day) {
    let hours = Math.floor(day.count / 60)
    let minutes = Math.floor(day.count % 60)
    this.setState({ selectedDate: day.date, selectedHours: hours, selectedMinutes: minutes })
  }
}

const styles = StyleSheet.create({
  cardTitleText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 20,
  },
  selectedDayDetailsWrapper: {
    flexDirection: 'row',
  },
  selectedDayData: {
    marginHorizontal: 5,
  },
  selectedDayHeading: {
    fontWeight: 'bold',
  },
})

export default AppContributionGraph
