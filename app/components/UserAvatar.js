import React from 'react'
import { StyleSheet, Image, View, Dimensions } from 'react-native'
import colors from '../styles/colors'
import UserLevel from './UserLevel'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

function UserAvatar(props) {
  return (
    <View style={styles.avatarContainer}>
      <Image
        style={styles.avatar}
        source={{ uri: `${props.user.avatar_url}?s=300` }}
      />
      <UserLevel user={props.user} />
    </View>
  )
}

const styles = StyleSheet.create({
  avatar: {
    width: windowWidth / 2,
    height: windowWidth / 2,
    borderRadius: windowWidth / 4,
    marginBottom: 10,
  },
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default UserAvatar
