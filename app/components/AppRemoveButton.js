import React from 'react'
import { StyleSheet } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import colors from '../styles/colors'

import AppButtonWithoutText from './AppButtonWithoutText'

function AppRemoveButton(props) {
  return (
    <AppButtonWithoutText style={styles.removeModalButton} onPress={props.onPress}>
      <FontAwesome5 name="trash" color={colors.light} size={22} />
    </AppButtonWithoutText>
  )
}

const styles = StyleSheet.create({
  removeModalButton: {
    paddingVertical: 5,
    backgroundColor: colors.red,
  },
})

export default AppRemoveButton
