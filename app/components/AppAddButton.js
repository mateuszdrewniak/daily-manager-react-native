import React from 'react'
import { StyleSheet } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import colors from '../styles/colors'

import AppButtonWithoutText from './AppButtonWithoutText'

function AppAddButton(props) {
  return (
    <AppButtonWithoutText style={styles.addButton} onPress={props.onPress}>
      <FontAwesome5 name="plus" color={colors.light} size={22} />
    </AppButtonWithoutText>
  )
}

const styles = StyleSheet.create({
  addButton: {
    paddingVertical: 5,
    paddingHorizontal: 8,
  },
})

export default AppAddButton
