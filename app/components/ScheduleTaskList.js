import React from 'react'
import { ScrollView, View, StyleSheet } from 'react-native'
import uuid from 'react-native-uuid'

import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import Card from './Card'
import AppAddButton from './AppAddButton'
import TaskCard from './TaskCard'
import AppText from './AppText'

const priorityNames = ['LOW', 'MEDIUM', 'HIGH']

function ScheduleTaskList(props) {
  let priority = props.priority || 0
  let tasks = props.tasks || []
  let alreadyAddedTaskIds = props.alreadyAddedTaskIds || []

  return (
    <Card style={[mainStyles.fullWidthCard, styles.taskListCard]}>
      <View style={styles.taskListCardTop}>
        <View style={styles.priorityContainer}>
          <AppText style={styles.priorityText}>{priorityNames[priority]}</AppText>
        </View>
        <AppAddButton onPress={props.onAddTaskPress}/>
      </View>
      <ScrollView>
        {
          tasks.map((task) => (
            <TaskCard
              key={uuid.v4()}
              onPress={() => props.onTaskPress ? props.onTaskPress(task) : null}
              onLongPress={props.onLongTaskPress}
              addingDailyTask={props.addingDailyTask}
              ableToAddToDay={!alreadyAddedTaskIds.includes(task.id)}
              onRemoveTaskPress={(task) => props.onRemoveTaskPress(task, priority)}
              task={task}
              {...task}
            />
          ))
        }
      </ScrollView>
    </Card>
  )
}

const styles = StyleSheet.create({
  taskListCard: {
    backgroundColor: colors.lightMain,
  },
  taskListCardTop: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: -5,
  },
  priorityText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
  },
  priority: {
    marginRight: 3,
  },
  priorityContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  addTaskButton: {
    paddingVertical: 7,
  },
})

export default ScheduleTaskList
