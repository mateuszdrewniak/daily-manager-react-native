import React from 'react'
import { View, StyleSheet } from 'react-native'
import colors from '../styles/colors'

import mainStyles from '../styles/mainStyles'
import AppText from './AppText'

function AppDisabledTextInput(props) {
  return (
    <View style={mainStyles.inputGroup}>
      <AppText style={mainStyles.inputLabel}>{props.label}</AppText>
      <View style={[mainStyles.textInput, styles.fakeTextInput]}>
        <AppText>{props.value}</AppText>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  fakeTextInput: {
    paddingVertical: 8,
    backgroundColor: colors.lightGray,
  },
})

export default AppDisabledTextInput
