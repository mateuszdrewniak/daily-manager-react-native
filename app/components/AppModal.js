import React from 'react'
import { View, StyleSheet, Modal, Pressable } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'

import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'

import Card from './Card'
import AppText from './AppText'
import AppButtonWithoutText from './AppButtonWithoutText'
import AppCloseButton from './AppCloseButton'


function AppModal(props) {
  return (
    <Modal
      animationType="slide"
      visible={props.visible}
      transparent
      onRequestClose={props.onRequestClose}
    >
      <Card style={styles.modalCard}>
        <View style={styles.modalTop}>
          <AppText style={styles.modalTitle}>{props.title}</AppText>
          <AppCloseButton onPress={props.onRequestClose}/>
        </View>
        { props.children }
      </Card>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalCard: {
    margin: 20,
  },
  modalTop: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 15,
  },
  modalTitle: {
    fontSize: 26,
  },
})

export default AppModal
