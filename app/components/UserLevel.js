import React from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'

import colors from '../styles/colors'
import AppText from './AppText'

function UserLevel(props) {
  const levelObject = { ...props.user.level_object, level: props.user.level }
  return (
    <View style={[styles.levelContainer, { backgroundColor: levelObject.background_color.hex }]}>
      <FontAwesome5 name={levelObject.icon} color={levelObject.color.hex} size={26} />
      <AppText style={[styles.levelText, { color: levelObject.color.hex }]}>{levelObject.level}</AppText>
    </View>
  )
}

const styles = StyleSheet.create({
  levelContainer: {
    borderRadius: 5,
    borderColor: colors.black,
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    maxWidth: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  levelText: {
    fontSize: 24,
    marginLeft: 5,
  },
})

export default UserLevel
