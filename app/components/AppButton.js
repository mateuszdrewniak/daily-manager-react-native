import React from 'react'
import { Pressable, StyleSheet } from 'react-native'
import colors from '../styles/colors'
import AppText from './AppText'

function AppButton(props) {
  let rippleConfig = { color: colors.lightMain, ...props.android_ripple }
  let buttonStyle = props.outline ? styles.outline : styles.solid
  let textStyle = props.outline ? styles.outlineText : styles.solidText

  return (
    <Pressable onPress={props.onPress} onLongPress={props.onLongPress} android_ripple={rippleConfig} style={[styles.button, buttonStyle, props.style]}>
      <AppText style={[styles.text, textStyle, props.labelStyle]}>{props.label}</AppText>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    marginVertical: 5,
    elevation: 3,
    borderRadius: 5,
  },
  solid: {
    backgroundColor: colors.main,
  },
  outline: {
    backgroundColor: colors.light,
    borderColor: colors.main,
    borderWidth: 1,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
  },
  solidText: {
    color: colors.light,
  },
  outlineText: {
    color: colors.main,
  },
})

export default AppButton
