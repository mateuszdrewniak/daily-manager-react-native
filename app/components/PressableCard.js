import React from 'react'
import { Pressable } from 'react-native'
import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'

function PressableCard(props) {
  let rippleConfig = props.onPress || props.onLongPress ? { color: colors.gray, ...props.android_ripple } : null

  return (
    <Pressable style={[mainStyles.card, props.style]} android_ripple={rippleConfig} onPress={props.onPress} onLongPress={props.onLongPress}>
      { props.children }
    </Pressable>
  )
}

export default PressableCard
