import React from 'react'
import { View } from 'react-native'
import mainStyles from '../styles/mainStyles'

function Card(props) {
  return (
    <View style={[mainStyles.card, props.style]}>
      { props.children }
    </View>
  )
}

export default Card
