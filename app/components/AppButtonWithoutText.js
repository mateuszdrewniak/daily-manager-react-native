import React from 'react'
import { Pressable, StyleSheet, Text } from 'react-native'
import colors from '../styles/colors'

function AppButtonWithoutText(props) {
  let rippleConfig = { color: colors.lightMain, ...props.android_ripple }
  let buttonStyle = props.outline ? styles.outline : styles.solid

  return (
    <Pressable parent={props.parent} onPress={props.onPress} onLongPress={props.onLongPress} android_ripple={rippleConfig} style={[styles.button, buttonStyle, props.style]}>
      { props.children }
    </Pressable>
  )
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    marginVertical: 5,
    elevation: 3,
    borderRadius: 5,
  },
  solid: {
    backgroundColor: colors.main,
  },
  outline: {
    backgroundColor: colors.light,
    borderColor: colors.main,
    borderWidth: 1,
  },
})

export default AppButtonWithoutText
