import DateAndTime from 'date-and-time'

class TimeHelper {
  static percentage(spentTime, totalTime) {
    if(!spentTime || !totalTime) return 0
  
    let spentSeconds = this.timeToSeconds(spentTime)
    let totalSeconds = this.timeToSeconds(totalTime)
    let percent = (spentSeconds / totalSeconds) * 100
    if(percent > 100) percent = 100
  
    return parseFloat(percent.toFixed(2))
  }

  static today() {
    return this.toIso(new Date())
  }

  static toIso(date) {
    return DateAndTime.format(date, 'YYYY-MM-DD')
  }
  
  static timeToSeconds(time) {
    if(!time) return 1
  
    let timeArray = time.split(':').map(Number)
    if(timeArray.length != 3) return 1
  
    let seconds = timeArray[0] * 3600 + timeArray[1] * 60 + timeArray[2]
  
    return seconds
  }

  static secondsToTime(seconds) {
    if(!seconds) return '00:00:00'
  
    let hours = Math.floor(seconds / 3600)
    currentSeconds = seconds % 3600
    let minutes = Math.floor(currentSeconds / 60)
    currentSeconds = currentSeconds % 60

    let time = [hours, minutes, currentSeconds]

    for(let i = 0; i < 3; i++) {
      time[i] = time[i] < 10 ? '0' + time[i] : time[i].toString()
    }
  
    return time.join(':')
  }
}

export default TimeHelper
