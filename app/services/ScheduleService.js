import Constants from 'expo-constants'
import RestRequest from './RestRequest'
import SecureStorageService from './SecureStorageService'

class ScheduleService {
  static async fetch() {
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/schedule.json`,
      headers: { 'Authorization': authorization },
      method: 'GET',
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }

  static async createTask(task) {
    let body = {
      name: task.name,
      priority_id: task.priority,
      time: task.time,
    }
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/tasks.json`,
      headers: { 'Authorization': authorization },
      body: body
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }

  static async updateTask(task) {
    let body = {
      id: task.id,
      name: task.name,
      priority_id: task.priority,
      time: task.time,
    }
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/tasks.json`,
      headers: { 'Authorization': authorization },
      body: body,
      method: 'PUT',
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }

  static async deleteTask(taskId) {
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/tasks/${taskId}.json`,
      headers: { 'Authorization': authorization },
      method: 'DELETE',
    })

    try {
      return await persistRequest.send()
    } catch(e) {
      return
    }
  }

  static async createDailyTask(taskId, day) {
    let body = {
      task_id: taskId,
      day: day,
    }
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/daily_tasks.json`,
      headers: { 'Authorization': authorization },
      body: body
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }

  static async deleteDailyTask(dailyTaskId) {
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/daily_tasks/${dailyTaskId}.json`,
      headers: { 'Authorization': authorization },
      method: 'DELETE',
    })

    try {
      return await persistRequest.send()
    } catch(e) {
      return
    }
  }
}

export default ScheduleService
