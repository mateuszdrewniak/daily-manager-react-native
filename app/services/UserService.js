import Constants from 'expo-constants'
import RestRequest from './RestRequest'
import SecureStorageService from './SecureStorageService'

class UserService {
  static async fetch() {
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/users.json`,
      headers: { 'Authorization': authorization },
      method: 'GET',
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }

  static async updateUser(user) {
    let body = user
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/users.json`,
      headers: { 'Authorization': authorization },
      body: body,
      method: 'PUT',
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }
}

export default UserService
