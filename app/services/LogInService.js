import Constants from 'expo-constants'
import RestRequest from './RestRequest'

class LogInService {
  constructor(email, password) {
    this.email = email
    this.password = password
  }

  async authenticate() {
    let authRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/sessions.json`,
      body: { user: { email: this.email, password: this.password } }
    })

    return authRequest.send()
  }
}

export default LogInService
