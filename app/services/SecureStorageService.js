import * as SecureStore from 'expo-secure-store'

export default class SecureStorageService {
  static async setAuthorization(value) {
    return await SecureStore.setItemAsync('dailyManagerAuthorization', value)
  }

  static async getAuthorization() {
    return await SecureStore.getItemAsync('dailyManagerAuthorization')
  }

  static async deleteAuthorization() {
    return await SecureStore.deleteItemAsync('dailyManagerAuthorization')
  }
}
