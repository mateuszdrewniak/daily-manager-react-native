import Constants from 'expo-constants'
import RestRequest from './RestRequest'
import SecureStorageService from './SecureStorageService'
import TimeHelper from './TimeHelper'

class TodayTasksService {
  static async fetch() {
    return new Promise((resolve, reject) => {
      SecureStorageService.getAuthorization().then(authorization => {
        let todayRequest = new RestRequest({
          url: `${Constants.manifest.extra.dailyManagerHost}/api/today.json`,
          headers: { 'Authorization': authorization },
          method: 'GET',
        })

        todayRequest.send().then(response => {
          if(response.status === 200 || response.status === 201) {
            response.json().then(body => { resolve(body) })
                           .catch(() => { reject(response) })
            return
          }

          reject(response)
        }).catch(() => { reject(null) })
      }).catch(() => { reject(null) })
    })
  }

  static async persistTask(task) {
    let authorization = await SecureStorageService.getAuthorization()
    let body = {
      date: TimeHelper.today(),
      spent_time: task.spent_time,
      completed: task.completed,
    }
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/daily_task_completions/${task.daily_task_id}.json`,
      headers: { 'Authorization': authorization },
      body: body,
      method: 'PUT',
    })

    try {
      let response = await persistRequest.send()
      return response
    } catch(e) {
      return
    }
  }
}

export default TodayTasksService
