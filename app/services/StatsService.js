import Constants from 'expo-constants'
import RestRequest from './RestRequest'
import SecureStorageService from './SecureStorageService'

class StatsService {
  static async fetch() {
    let authorization = await SecureStorageService.getAuthorization()
    let persistRequest = new RestRequest({
      url: `${Constants.manifest.extra.dailyManagerHost}/api/stats.json`,
      headers: { 'Authorization': authorization },
      method: 'GET',
    })

    try {
      let response = await persistRequest.send()
      return [await response.json(), response]
    } catch(e) {
      return
    }
  }
}

export default StatsService
