
class RestRequest {
  constructor({ url, body = null, method = 'POST', headers = {} }) {
    this.url = url
    this.body = body
    this.method = method
    this.headers = headers
  }

  async send() {
    let request = {
      method: this.method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...this.headers
      }
    }

    if(this.body) request.body = JSON.stringify(this.body)

    let response = await fetch(this.url, request)

    this.response = response

    return this.response
  }
}

export default RestRequest