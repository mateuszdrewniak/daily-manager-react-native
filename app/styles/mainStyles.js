import { Dimensions, StyleSheet } from 'react-native'
import colors from "./colors"

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

const mainStyles = StyleSheet.create({
  logo: {
    fontFamily: 'Yellowtail',
    color: colors.main,
    marginBottom: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 25,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
    textDecorationColor: colors.black,
    color: colors.black,
    marginVertical: 20,
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  textInput: {
    paddingHorizontal: 5,
    marginTop: 5,
    paddingVertical: 2,
    borderColor: colors.gray,
    borderWidth: 1,
    borderRadius: 2,
    elevation: 0,
  },
  inputGroup: {
    marginVertical: 5,
  },
  fullWidthCard: {
    width: windowWidth - 30,
    marginHorizontal: 15,
    marginBottom: 20,
  },
  card: {
    backgroundColor: colors.light,
    padding: 15,
    elevation: 5,
    borderRadius: 5,
    margin: 5,
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  textInput: {
    paddingHorizontal: 5,
    marginTop: 5,
    paddingVertical: 2,
    borderColor: colors.gray,
    backgroundColor: colors.light,
    borderWidth: 1,
    borderRadius: 4,
    elevation: 3,
  },
  inputGroup: {
    marginVertical: 5,
    // marginHorizontal: 5,
  },
  modalSubmitButton: {
    marginTop: 20,
  },
})

export default mainStyles
