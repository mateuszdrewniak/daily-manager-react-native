
import React, { Component } from 'react'
import { ScrollView, View, StyleSheet, RefreshControl } from 'react-native'
import uuid from 'react-native-uuid'
import { Audio } from 'expo-av'
import * as Notifications from 'expo-notifications'

import Container from '../components/Container'
import Card from '../components/Card'
import DateAndTime from 'date-and-time'
import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'
import { FontAwesome5 } from '@expo/vector-icons'
import TodayTasksService from '../services/TodayTasksService'
import LoadingScreen from './LoadingScreen'
import NoConnectionScreen from './NoConnectionScreen'
import FullTaskCard from '../components/FullTaskCard'
import AppText from '../components/AppText'

import TimeHelper from '../services/TimeHelper'

class TodayScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      toDoPercent: 0,
      donePercent: 0,
      connection: true,
      fetched: false,
      toDoTasks: {},
      doneTasks: {},
      lastTimeMeasurement: null,
      currentInterval: -1,
      currentTask: {},
      currentFinishNotification: null
    }
    this.horizontalScrollRef = React.createRef()
    this.handlePressOnTask = this.handlePressOnTask.bind(this)
  }

  render() {
    if(!this.state.connection) {
      let todayScreen = this
      setTimeout(() => {
        todayScreen.fetch()
      }, 1500)
      return <NoConnectionScreen/>
    }

    if(!this.state.fetched) {
      this.fetchIfNecessary()
      return <LoadingScreen/>
    }

    return (
      <Container>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => this.setState({ fetched: false })}
            />
          }
        >
          <View>
            <AppText style={mainStyles.title}>{this.date()}</AppText>
          </View>
          <ScrollView ref={this.horizontalScrollRef} horizontal pagingEnabled>
            <Card style={[mainStyles.fullWidthCard, styles.taskListCard, styles.toDoCard]}>
              <View style={styles.taskListCardTop}>
                <AppText style={styles.taskListCardTitle}>TO DO</AppText>
                <AppText style={[styles.taskListCardPercent, this.toDoPercentColor()]}>{this.state.toDoPercent}%</AppText>
              </View>
              {this.renderToDoTasks()}
            </Card>

            <Card style={[mainStyles.fullWidthCard, styles.taskListCard, styles.doneCard]}>
              <View style={styles.taskListCardTop}>
                <AppText style={styles.taskListCardTitle}>DONE</AppText>
                <AppText style={[styles.taskListCardPercent, this.donePercentColor()]}>{this.state.donePercent}%</AppText>
              </View>
              {this.renderDoneTasks()}
            </Card>
          </ScrollView>
        </ScrollView>
      </Container>
    )
  }

  toDoPercentColor() {
    if(!this.state.toDoPercent) return styles.progress100

    let toDoPercentStyles = ['progress100', 'progress75', 'progress50', 'progress25', 'progress0']
    return styles[toDoPercentStyles[Math.floor(this.state.toDoPercent / 25)]]
  }

  donePercentColor() {
    if(!this.state.donePercent) return styles.progress0

    let donePercentStyles = ['progress0', 'progress25', 'progress50', 'progress75', 'progress100']
    return styles[donePercentStyles[Math.floor(this.state.donePercent / 25)]]
  }

  renderToDoTasks() {
    if(this.state.toDoTasks === null || this.state.toDoTasks.length === 0) return this.renderEmptyToDo()

    let toDoTasks = this.state.toDoTasks.map(taskData => (
      <FullTaskCard {...taskData} key={uuid.v4()} onPress={() => this.handlePressOnTask(taskData)} />
    ))

    return (
      <ScrollView>
        {toDoTasks}
      </ScrollView>
    )
  }

  renderDoneTasks() {
    if(this.state.doneTasks === null || this.state.doneTasks.length === 0) return this.renderEmptyDone()

    let doneTasks = this.state.doneTasks.map(taskData => (
      <FullTaskCard done {...taskData} key={uuid.v4()} onPress={() => this.handlePressOnTask(taskData)} />
    ))

    return (
      <ScrollView>
        {doneTasks}
      </ScrollView>
    )
  }

  handlePressOnTask(task) {
    let newState = {}
    let taskGroupNameKey = `${task.completed ? 'done' : 'toDo'}Tasks`
    newState[taskGroupNameKey] = this.state[taskGroupNameKey]

    this.stopCurrentTask()
    if(task.daily_task_id === this.state.currentTask.daily_task_id) return

    newState.currentTask = task
    task.current = true
    newState.lastTimeMeasurement = new Date()
    newState.currentInterval = setInterval(() => this.measureTime(task), 1000)

    if(!task.completed) {
      let secondsLeft = TimeHelper.timeToSeconds(task.time) - TimeHelper.timeToSeconds(task.spent_time)
      let endTime = DateAndTime.format(new Date(newState.lastTimeMeasurement - 0 + secondsLeft * 1000), 'HH:mm:ss')
      Notifications.scheduleNotificationAsync({
        content: {
          title: task.name,
          body: `Task will end at ${endTime}`,
        },
        trigger: null
      })

      Notifications.scheduleNotificationAsync({
        content: {
          title: `${task.name} Finished!`,
          body: `Congratulations, you've just finished your task!`,
        },
        trigger: {
          seconds: secondsLeft,
        },
      }).then((id) => this.state.currentFinishNotification = id)
    }

    this.setState(newState)
  }

  stopCurrentTask() {
    let task = this.state.currentTask
    task.current = null
    if(this.state.currentFinishNotification) Notifications.cancelScheduledNotificationAsync(this.state.currentFinishNotification)
    clearInterval(this.state.currentInterval)

    if(task.daily_task_id) TodayTasksService.persistTask(task)

    let newState = { currentInterval: null, currentTask: {}, lastTimeMeasurement: null, currentFinishNotification: null }
    this.setState(newState)
  }

  measureTime(task) {
    let newState = { doneTasks: this.state.doneTasks, toDoTasks: this.state.toDoTasks }

    let spentSeconds = TimeHelper.timeToSeconds(task.spent_time)
    let currentDate = new Date()
    let difference = (currentDate - this.state.lastTimeMeasurement) / 1000
    difference = difference > 1.5 ? Math.round(difference) : 1
    spentSeconds += difference
    task.spent_time = TimeHelper.secondsToTime(spentSeconds)
    let totalSeconds = TimeHelper.timeToSeconds(task.time)

    if(!task.completed && spentSeconds >= totalSeconds) {
      task.completed = true
      newState.toDoTasks.splice(newState.toDoTasks.indexOf(task), 1)
      newState.doneTasks.unshift(task)
      let allTasksCount = this.state.toDoTasks.length + this.state.doneTasks.length
      newState.toDoPercent = this.percent(this.state.toDoTasks.length, allTasksCount)
      newState.donePercent = this.percent(this.state.doneTasks.length, allTasksCount)
      this.horizontalScrollRef.current.scrollToEnd({ animated: true })
      this.playTaskDoneSound()
      if(newState.donePercent == 100) this.playAllDoneSound()
    }

    newState.lastTimeMeasurement = currentDate
    this.setState(newState)
  }

  async playTaskDoneSound() {
    const sound = new Audio.Sound()
    await sound.loadAsync(require('./../assets/sounds/task-done.mp3'))
    await sound.playAsync()
    setTimeout(() => { sound.unloadAsync() }, 4000)
  }

  async playAllDoneSound() {
    const sound = new Audio.Sound()
    await sound.loadAsync(require('./../assets/sounds/all-done.mp3'))
    await sound.playAsync()
    setTimeout(() => { sound.unloadAsync() }, 4000)
  }

  fetchIfNecessary() {
    if(this.state.fetched) return

    this.fetch()
  }

  fetch() {
    let todayScreen = this

    TodayTasksService.fetch().then((json) => {
      let allTasksCount = json.todo_tasks.length + json.done_tasks.length
      
      let toDoPercent = this.percent(json.todo_tasks.length, allTasksCount)
      let donePercent = this.percent(json.done_tasks.length, allTasksCount)
      todayScreen.setState({ fetched: true, toDoTasks: json.todo_tasks, doneTasks: json.done_tasks, toDoPercent: toDoPercent, donePercent: donePercent })
    }).catch(() => {
      if(todayScreen.state.connection) todayScreen.setState({ connection: false })
    })
  }

  renderEmptyToDo() {
    return (
      <View style={styles.emptyListIconContainer}>
        <FontAwesome5 style={styles.emptyListIcon} name="crown" color={colors.black} size={46} />
        <AppText style={styles.emptyListIconText}>No tasks left!</AppText>
      </View>
    )
  }

  renderEmptyDone() {
    return (
      <View style={styles.emptyListIconContainer}>
        <FontAwesome5 style={styles.emptyListIcon} name="cloud-moon" color={colors.black} size={46} />
        <AppText style={styles.emptyListIconText}>Nothing done yet..</AppText>
      </View>
    )
  }

  percent(value, baseValue) {
    return parseFloat((value / baseValue * 100).toFixed(1))
  }

  date() {
    let now = new Date()
    return DateAndTime.format(now, 'dddd, DD MMMM')
  }
}

const styles = StyleSheet.create({
  toDoCard: {
    backgroundColor: colors.group,

  },
  doneCard: {
    backgroundColor: colors.lightMain,
  },
  taskListCard: {
    minHeight: 200,
  },
  taskListCardTop: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 10,
  },
  taskListCardPercent: {
    fontSize: 20,
    backgroundColor: colors.red,
    color: colors.light,
    elevation: 2,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
  progress0: {
    backgroundColor: colors.red,
    color: colors.light,
  },
  progress25: {
    backgroundColor: colors.warning,
    color: colors.light,
  },
  progress50: {
    backgroundColor: colors.lightYellow,
    color: colors.black,
  },
  progress75: {
    backgroundColor: colors.lightSecondary,
    color: colors.light,
  },
  progress100: {
    backgroundColor: colors.lightGreen,
    color: colors.black,
  },
  taskListCardTitle: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.black,
  },
  emptyListIconContainer: {
    flex: 1,
    justifyContent: 'center',

  },
  emptyListIcon: {
    textAlign: 'center',
    fontSize: 50,
    textAlignVertical: 'center',
  },
  emptyListIconText: {
    textAlign: 'center',
    fontSize: 25,
    textAlignVertical: 'center',
    color: colors.black,
  },
})

export default TodayScreen
