
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'

import { FontAwesome5 } from '@expo/vector-icons'

import colors from '../styles/colors'
import Container from '../components/Container'
import AppText from '../components/AppText'

class NoConnectionScreen extends Component {
  render() {
    return (
      <Container>
        <View style={styles.noConnectionWrapper}>
          <FontAwesome5 style={styles.noConnectionIcon} name="satellite" color={colors.black} size={150} />
          <AppText style={styles.noConnectionText}>Server doesn't respond</AppText>
          <AppText style={styles.noConnectionText}>Check your connection</AppText>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  noConnectionWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noConnectionIcon: {

  },
  noConnectionText: {
    fontSize: 25,
    color: colors.black,
  },
})

export default NoConnectionScreen
