
import React, { Component } from 'react'
import { View, Text, StyleSheet, ActivityIndicator, Image } from 'react-native'

import colors from '../styles/colors'
import Container from '../components/Container'

class LoadingScreen extends Component {
  render() {
    return (
      <Container>
        <View style={styles.loadingWrapper}>
          <Image style={styles.logo} source={require('../assets/title.png')} />
          <ActivityIndicator size="large" color={colors.secondary} />
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  loadingWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 300,
    resizeMode: 'contain',
  },
})

export default LoadingScreen
