
import React, { Component } from 'react'
import { View, Dimensions, StyleSheet, ToastAndroid, ScrollView } from 'react-native'

import LoadingScreen from './LoadingScreen'
import NoConnectionScreen from './NoConnectionScreen'

import mainStyles from '../styles/mainStyles'

import Container from '../components/Container'
import AppText from '../components/AppText'
import StatsService from '../services/StatsService'
import colors from '../styles/colors'
import AppBarChart from '../components/AppBarChart'
import AppLineChart from '../components/AppLineChart'
import AppPieChart from '../components/AppPieChart'
import AppContributionGraph from '../components/AppContributionGraph'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

class StatsScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      connection: true,
      fetched: false,
    }
  }

  render() {
    if(!this.state.connection) {
      setTimeout(() => this.fetch(), 1500)
      return <NoConnectionScreen/>
    }

    if(!this.state.fetched) {
      this.fetch()
      return <LoadingScreen/>
    }

    return (
      <Container>
        <View>
          <AppText style={mainStyles.title}>Stats</AppText>
        </View>
        <ScrollView>
          <AppText style={styles.statsGroupText}>Last months</AppText>
          <AppContributionGraph
            title='Last two months of activity'
            endDate={new Date(this.state.last_months.contribution_graph[this.state.last_months.contribution_graph.length - 1].date)}
            numDays={this.state.last_months.contribution_graph.length}
            data={this.state.last_months.contribution_graph}
          />
          <AppText style={styles.statsGroupText}>Tasks per day</AppText>
          <AppPieChart title='Tasks count per priority' data={this.state.tasks_count_per_priority}/>
          <AppBarChart title="Tasks assigned per day" data={this.state.tasks_per_day.daily_tasks_count}/>
          <AppBarChart color={colors.secondary} title="Hours of assigned tasks per day" data={this.state.tasks_per_day.assigned_tasks_hours}/>
          <AppText style={styles.statsGroupText}>This week</AppText>
          <AppLineChart title="Hours spent on tasks" data={this.state.this_week.hours_spent_on_tasks}/>
          <AppLineChart width={windowWidth * 2} verticalLabelRotation={0} title="Hours spent per task" data={this.state.this_week.hours_spent_per_task}/>
          <AppText style={styles.statsGroupText}>Last week</AppText>
          <AppLineChart title="Hours spent on tasks" data={this.state.last_week.hours_spent_on_tasks}/>
          <AppLineChart width={windowWidth * 2} verticalLabelRotation={0} title="Hours spent per task" data={this.state.last_week.hours_spent_per_task}/>
        </ScrollView>
        
      </Container>
    )
  }

  fetch() {
    let statsScreen = this
    StatsService.fetch().then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        statsScreen.setState({ fetched: true, ...json })
        return
      }

      ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }
}

const styles = StyleSheet.create({
  statsGroupText: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
    marginLeft: 10,
  },
  chartCard: {

  },
  
})

export default StatsScreen
