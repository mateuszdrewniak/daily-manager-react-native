
import React, { Component } from 'react'
import { Dimensions, View, ScrollView, StyleSheet, ToastAndroid } from 'react-native'
import uuid from 'react-native-uuid'
import * as Haptics from 'expo-haptics'

import Container from '../components/Container'
import AppText from '../components/AppText'
import ScheduleTaskList from '../components/ScheduleTaskList'
import ScheduleDailyTaskList from '../components/ScheduleDailyTaskList'
import AppTextInput from '../components/AppTextInput'
import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'

import LoadingScreen from './LoadingScreen'
import NoConnectionScreen from './NoConnectionScreen'

import ScheduleService from '../services/ScheduleService'
import AppModal from '../components/AppModal'
import AppTimePicker from '../components/AppTimePicker'
import AppRadioInput from '../components/AppRadioInput'
import AppButton from '../components/AppButton'

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height
const priorities = [{ label: 'Low', value: 0 }, { label: 'Medium', value: 1 }, { label: 'High', value: 2 }]

class ScheduleScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      connection: true,
      fetched: false,
      priorities: null,
      days: null,
      addTaskModalVisible: false,
      editTaskModalVisible: false,
      addingDailyTask: -1
    }

    this.addTaskData = {
      time: '00:30',
      name: null,
      priority: 0,
    }

    this.editTaskData = {
      time: '00:30',
      name: null,
      priority: 0,
    }

    this.onAddTaskPress = this.onAddTaskPress.bind(this)
    this.createTask = this.createTask.bind(this)
    this.onAddDailyTaskPress = this.onAddDailyTaskPress.bind(this)
    this.onQuitAddingDailyTaskPress = this.onQuitAddingDailyTaskPress.bind(this)
    this.onTaskPress = this.onTaskPress.bind(this)
    this.onLongDailyTaskPress = this.onLongDailyTaskPress.bind(this)
    this.onRemoveDailyTaskPress = this.onRemoveDailyTaskPress.bind(this)
    this.updateTask = this.updateTask.bind(this)
    this.deleteTask = this.deleteTask.bind(this)
    this.onLongTaskPress = this.onLongTaskPress.bind(this)
    this.onRemoveTaskPress = this.onRemoveTaskPress.bind(this)

    this.prioritiesScrollRef = React.createRef()
  }

  render() {
    if(!this.state.connection) {
      setTimeout(() => this.fetch(), 1500)
      return <NoConnectionScreen/>
    }

    if(!this.state.fetched) {
      this.fetch()
      return <LoadingScreen/>
    }

    return (
      <Container>
        <AppModal title='Create Task' visible={this.state.addTaskModalVisible} onRequestClose={() => this.setState({ addTaskModalVisible: false })}>
          <AppTextInput
            onChangeText={text => { this.addTaskData.name = text }}
            label='Name'
          />
          <AppRadioInput label='Priority' data={priorities} initial={this.addTaskData.priority + 1} onChange={(element) => this.addTaskData.priority = element.value}/>
          <AppTimePicker onChange={(time) => { this.addTaskData.time = time }}/>
          <AppButton label='Create' style={mainStyles.modalSubmitButton} onPress={this.createTask}/>
        </AppModal>
        <AppModal title='Update Task' visible={this.state.editTaskModalVisible} onRequestClose={() => this.setState({ editTaskModalVisible: false })}>
          <AppTextInput
            defaultValue={this.editTaskData.name}
            onChangeText={text => { this.editTaskData.name = text }}
            label='Name'
          />
          <AppRadioInput label='Priority' data={priorities} initial={this.editTaskData.priority + 1} onChange={(element) => this.editTaskData.priority = element.value}/>
          <AppTimePicker time={this.editTaskData.time} onChange={(time) => { this.editTaskData.time = time }}/>
          <View style={styles.editTaskModalSubmitContainer}>
            <AppButton label='Delete' style={[mainStyles.modalSubmitButton, styles.modalActionButton, styles.modalDeleteButton]} onPress={this.deleteTask}/>
            <AppButton label='Update' style={[mainStyles.modalSubmitButton, styles.modalActionButton]} onPress={this.updateTask}/>
          </View>
        </AppModal>
        <View>
          <AppText style={mainStyles.title}>Schedule</AppText>
        </View>
        <ScrollView ref={this.prioritiesScrollRef} style={styles.taskListScrollView} horizontal pagingEnabled>
          {
            this.state.priorities.map((priority, i) => (
              <ScheduleTaskList
                key={uuid.v4()}
                onTaskPress={this.onTaskPress}
                addingDailyTask={this.state.addingDailyTask != -1}
                alreadyAddedTaskIds={this.state.alreadyAddedTaskIds}
                priority={i}
                onAddTaskPress={() => this.onAddTaskPress(i)}
                onLongTaskPress={this.onLongTaskPress}
                onRemoveTaskPress={this.onRemoveTaskPress}
                tasks={priority}
              />
            ))
          }
        </ScrollView>
        <ScrollView style={styles.dailyTaskListScrollView} horizontal pagingEnabled>
          {
            this.state.days.map((day, i) => (
              <ScheduleDailyTaskList
                onRemoveDailyTaskPress={this.onRemoveDailyTaskPress}
                onLongDailyTaskPress={this.onLongDailyTaskPress}
                addingDailyTask={this.state.addingDailyTask == i}
                onQuitAddingDailyTaskPress={this.onQuitAddingDailyTaskPress}
                onAddDailyTaskPress={() => this.onAddDailyTaskPress(i)}
                key={uuid.v4()}
                day={i}
                tasks={day}
              />
            ))
          }
        </ScrollView>
      </Container>
    )
  }

  fetch() {
    let scheduleScreen = this
    ScheduleService.fetch().then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        scheduleScreen.setState({ priorities: json.priorities, days: json.days, connection: true, fetched: true })
        return
      }

      scheduleScreen.setState({ connection: false })
    }).catch(() => this.setState({ connection: false }))
  }

  onAddTaskPress(priority) {
    this.addTaskData.priority = priority
    this.setState({ addTaskModalVisible: true })
  }

  createTask() {
    let scheduleService = this
    ScheduleService.createTask(this.addTaskData).then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        scheduleService.prioritiesScrollRef.current.scrollTo({ x: json.priority * windowWidth })
        scheduleService.state.priorities[json.priority].unshift(json)
        scheduleService.setState({ addTaskModalVisible: false })
        return
      }

      ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
    }).catch((e) => {
      ToastAndroid.show("An error has occurred", ToastAndroid.SHORT)
    })
  }

  onAddDailyTaskPress(day) {
    let alreadyAddedTaskIds = []
    for(let dailyTask of this.state.days[day]) alreadyAddedTaskIds.push(dailyTask.task_id)

    this.setState({ addingDailyTask: day, alreadyAddedTaskIds: alreadyAddedTaskIds })
  }

  onQuitAddingDailyTaskPress() {
    this.setState({ addingDailyTask: -1, alreadyAddedTaskIds: null })
  }

  onTaskPress(task) {
    let currentDay = this.state.addingDailyTask
    if(currentDay != -1) {
      let taskAlreadyPresent = this.state.alreadyAddedTaskIds.includes(task.id)

      if(taskAlreadyPresent) {
        ToastAndroid.show('This task is already linked to this day!', ToastAndroid.SHORT)
        return
      }

      let scheduleScreen = this
      ScheduleService.createDailyTask(task.id, currentDay).then(([json, response]) => {
        if(response && (response.status === 200 || response.status === 201)) {
          let newState = { days: scheduleScreen.state.days, alreadyAddedTaskIds: scheduleScreen.state.alreadyAddedTaskIds }
          newState.days[currentDay].unshift(json)
          newState.alreadyAddedTaskIds.push(json.task_id)
          scheduleScreen.setState(newState)
          return
        }

        ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
      }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))

      return
    }

    this.editTaskData = { ...task, originalTask: task }
    this.setState({ editTaskModalVisible: true })
  }

  onLongDailyTaskPress(task, taskCard) {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium)

    if(taskCard.state.actionsActive) {
      taskCard.setState({ actionsActive: false })
      return
    }

    taskCard.setState({ actionsActive: true })
  }

  onRemoveDailyTaskPress(dailyTask, day) {
    let currentDailyTasks = this.state.days[day]

    let scheduleScreen = this
    ScheduleService.deleteDailyTask(dailyTask.id).then((response) => {
      if(response && response.status === 200) {
        currentDailyTasks.splice(currentDailyTasks.indexOf(dailyTask), 1)
        scheduleScreen.setState(scheduleScreen.state)
        return
      }

      ToastAndroid.show("An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }

  onRemoveTaskPress(task, priority) {
    this.editTaskData = { ...task, originalTask: task }
    this.deleteTask()
  }

  updateTask() {
    let scheduleScreen = this
    ScheduleService.updateTask(this.editTaskData).then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        let newState = { editTaskModalVisible: false }
        let oldTaskGroup = scheduleScreen.state.priorities[scheduleScreen.editTaskData.originalTask.priority]
        oldTaskGroup.splice(oldTaskGroup.indexOf(scheduleScreen.editTaskData.originalTask), 1)

        scheduleScreen.state.priorities[json.priority].unshift(json)
        scheduleScreen.prioritiesScrollRef.current.scrollTo({ x: json.priority * windowWidth })
        scheduleScreen.setState(newState)
        return
      }

      ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }

  deleteTask() {
    let scheduleScreen = this
    ScheduleService.deleteTask(this.editTaskData.id).then(response => {
      if(response && (response.status === 200 || response.status === 201)) {
        let newState = { editTaskModalVisible: false }
        let oldTaskGroup = scheduleScreen.state.priorities[scheduleScreen.editTaskData.originalTask.priority]
        oldTaskGroup.splice(oldTaskGroup.indexOf(scheduleScreen.editTaskData.originalTask), 1)
        scheduleScreen.setState(newState)
        return
      }

      ToastAndroid.show("An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }

  onLongTaskPress(task, taskCard) {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium)

    if(taskCard.state.actionsActive) {
      taskCard.setState({ actionsActive: false })
      return
    }

    taskCard.setState({ actionsActive: true })
  }
}

const styles = StyleSheet.create({
  taskListScrollView: {
    flex: 1,
  },
  dailyTaskListScrollView: {
    flex: 1,
  },
  editTaskModalSubmitContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: -5,
  },
  modalDeleteButton: {
    backgroundColor: colors.red,
  },
  modalActionButton: {
    flex: 1,
    marginHorizontal: 5,
  },
})

export default ScheduleScreen
