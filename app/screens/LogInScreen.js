
import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid } from 'react-native'

import SecureStorageService from '../services/SecureStorageService'

import Container from '../components/Container'
import AppText from '../components/AppText'
import Card from '../components/Card'
import AppButton from '../components/AppButton'
import colors from '../styles/colors'
import mainStyles from '../styles/mainStyles'
import AppTextInput from '../components/AppTextInput'
import LogInService from '../services/LogInService'

class LogInScreen extends Component {
  constructor(props) {
    super(props)
    this.state = { email: '', password: '' }
    this.logInRequest = this.logInRequest.bind(this)
    this.analyseLogInResponse = this.analyseLogInResponse.bind(this)
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={styles.logInContainer}>
          <AppText style={[mainStyles.logo, styles.logo,]}>Daily Manager</AppText>
          <Card style={styles.logInCard}>
            <AppTextInput
              onChangeText={event => this.setState({ email: event })}
              label={'Email'}
              keyboardType={'email-address'}
              textContentType={'emailAddress'}
            />
            <AppTextInput
              onChangeText={event => this.setState({ password: event })}
              label={'Password'}
              secureTextEntry={true}
              textContentType={'password'}
              autoCompleteType={'password'}
            />
            <AppButton onPress={this.logInRequest} label={'Log In'}/>
          </Card>
        </View>
      </Container>
    )
  }

  logInRequest() {
    if(this.state.email === null || this.state.password === null) return

    let logInService = new LogInService(this.state.email, this.state.password)
    logInService.authenticate().then(this.analyseLogInResponse).catch(error => console.log('error'))
  }

  analyseLogInResponse(response) {
    if(response.status === 200 || response.status === 201) {
      let authorization = response.headers.get('Authorization')
      
      SecureStorageService.setAuthorization(authorization).then(() => {
        console.log(authorization)
        this.props.setSignedIn(true)
      }).catch((error) => {
        console.log(error)
        ToastAndroid.show("There was an error while saving your session. Please try again.", ToastAndroid.SHORT)
      })

      return
    }

    response.json().then(json => { ToastAndroid.show(json.error || 'Unknown error. Check your internet connection.', ToastAndroid.SHORT) })
                   .catch(error => { ToastAndroid.show('Invalid email or password. Check your internet connection.', ToastAndroid.SHORT) })
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
  },
  logo: {
    fontSize: 50,
    textAlign: 'center',
  },
  logInContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  logInCard: {
    minWidth: 300,
  },
})

export default LogInScreen
