
import React, { Component } from 'react'
import { View, ScrollView, StyleSheet, ToastAndroid, RefreshControl } from 'react-native'

import mainStyles from '../styles/mainStyles'
import LoadingScreen from './LoadingScreen'
import NoConnectionScreen from './NoConnectionScreen'

import Container from '../components/Container'
import AppText from '../components/AppText'
import AppTextInput from '../components/AppTextInput'
import UserAvatar from '../components/UserAvatar'
import UserService from '../services/UserService'
import AppDisabledTextInput from '../components/AppDisabledTextInput'
import AppButton from '../components/AppButton'
import colors from '../styles/colors'
import SecureStorageService from '../services/SecureStorageService'

class ProfileScreen extends Component {
  constructor(props) {
    super(props)

    this.editUserData = {}

    this.state = {
      connection: true,
      fetched: false,
    }

    this.logOut = this.logOut.bind(this)
    this.updateUser = this.updateUser.bind(this)
  }

  render() {
    if(!this.state.connection) {
      setTimeout(() => this.fetch(), 1500)
      return <NoConnectionScreen/>
    }

    if(!this.state.fetched) {
      this.fetch()
      return <LoadingScreen/>
    }

    return (
      <Container>
        <View>
          <AppText style={mainStyles.title}>My Account</AppText>
          <ScrollView
            style={styles.scrollView}
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => this.setState({ fetched: false })}
              />
            }
          >
            <View style={styles.formContainer}>
              <UserAvatar user={this.state.user}/>
              <AppTextInput
                defaultValue={this.state.user.display_name}
                onChangeText={text => { this.editUserData.display_name = text }}
                label='Display Name'
              />
              <AppTextInput
                defaultValue={this.state.user.email}
                onChangeText={text => { this.editUserData.email = text }}
                label='Email'
              />
              <AppDisabledTextInput
                label='Avatar Source'
                value={this.state.user.avatar_source}
              />
              <AppDisabledTextInput
                label='Account Type'
                value={this.state.user.account_type}
              />
              <AppDisabledTextInput
                label='Timezone'
                value={this.state.user.timezone}
              />
              <View style={styles.formSubmitContainer}>
                <AppButton
                  label='Update'
                  style={[styles.formSubmitButton]}
                  onPress={this.updateUser}
                />
                <AppButton
                  label='Log Out'
                  style={[styles.formSubmitButton, styles.logOutButton]}
                  onPress={this.logOut}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </Container>
    )
  }

  fetch() {
    let statsScreen = this
    UserService.fetch().then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        statsScreen.editUserData = { ...json }
        statsScreen.setState({ fetched: true, user: json })
        return
      }

      ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }

  updateUser() {
    let statsScreen = this
    UserService.updateUser(this.editUserData).then(([json, response]) => {
      if(response && (response.status === 200 || response.status === 201)) {
        statsScreen.editUserData = { ...json }
        statsScreen.setState({ user: json })

        if(json.email_change) {
          ToastAndroid.show("A confirmation mail has been sent to your new mail!", ToastAndroid.SHORT)
          statsScreen.logOut()
          return
        }

        ToastAndroid.show("Details updated!", ToastAndroid.SHORT)

        return
      }

      ToastAndroid.show(json.message || "An error has occurred", ToastAndroid.SHORT)
    }).catch(() => ToastAndroid.show("An error has occurred", ToastAndroid.SHORT))
  }

  logOut() {
    SecureStorageService.deleteAuthorization()
    this.props.app.setState({ signedIn: false })
  }
}

const styles = StyleSheet.create({
  scrollView: {
    // flex: 1,
  },
  formContainer: {
    marginHorizontal: 5,
    paddingHorizontal: 15,
    marginBottom: 90,
  },
  formSubmitContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: -5,
  },
  formSubmitButton: {
    flex: 1,
    marginHorizontal: 5,
  },
  logOutButton: {
    backgroundColor: colors.red,
  },
})

export default ProfileScreen
