import React, { Component } from 'react'
import { StyleSheet, Pressable, Text } from 'react-native'
import { useFonts } from 'expo-font'
import * as Notifications from 'expo-notifications'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import { FontAwesome5 } from '@expo/vector-icons'

import colors from './app/styles/colors'
import NavBarButton from './app/components/NavBarButton'
import SecureStorageService from './app/services/SecureStorageService'

import LoadingScreen from './app/screens/LoadingScreen'
import LogInScreen from './app/screens/LogInScreen'
import TodayScreen from './app/screens/TodayScreen'
import StatsScreen from './app/screens/StatsScreen'
import ScheduleScreen from './app/screens/ScheduleScreen'
import ProfileScreen from './app/screens/ProfileScreen'

const Tab = createBottomTabNavigator()

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
})

Notifications.setNotificationChannelAsync('DailyManager', {
  name: 'Daily Manager',
  importance: Notifications.AndroidImportance.HIGH,
  description: 'Notifications about your tasks',
  lightColor: colors.lightMain,
  showBadge: true,
})

class Navigation extends Component {
  constructor(props) {
    super(props)
    this.state = { signedIn: null }

    this.setSignedIn = this.setSignedIn.bind(this)

    let navigation = this
    // SecureStorageService.deleteAuthorization()
    SecureStorageService.getAuthorization().then((authorization) => {
      if(!authorization) {
        navigation.setState({ signedIn: false })
        return
      }
      console.log(authorization)

      navigation.setState({ signedIn: true })
    })
  }

  setSignedIn() {
    this.setState({ signedIn: true })
  }

  render() {
    if(this.state.signedIn === null) return <LoadingScreen/>
    
    if(this.state.signedIn === false) return <LogInScreen setSignedIn={this.setSignedIn}/>

    return (
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Today"
          activeColor={colors.light}
          inactiveColor={colors.lightGray}
          barStyle={styles.navigationBar}
          shifting={true}
          tabBarOptions={{
            style: styles.navigationBar,
            keyboardHidesTabBar: true,
            activeTintColor: colors.ultraLightSecondary,
            inactiveTintColor: colors.light,
          }}
        >
          <Tab.Screen name="Today" component={TodayScreen}
            options={{
              tabBarLabel: 'Today',
              tabBarIcon: ({ color }) => (
                <FontAwesome5 name="stopwatch" color={color} size={26} />
              ),
              tabBarButton: props => <NavBarButton {...props}/>,
            }}
          />
          <Tab.Screen name="Schedule" component={ScheduleScreen}
            options={{
              tabBarLabel: 'Schedule',
              tabBarIcon: ({ color }) => (
                <FontAwesome5 name="calendar" color={color} size={26} />
              ),
              tabBarButton: props => <NavBarButton {...props}/>,
            }}
          />
          <Tab.Screen name="Stats" component={StatsScreen}
            options={{
              tabBarLabel: 'Stats',
              tabBarIcon: ({ color }) => (
                <FontAwesome5 name="chart-bar" color={color} size={26} />
              ),
              tabBarButton: props => <NavBarButton {...props}/>,
            }}
          />
          <Tab.Screen name="Profile"
            options={{
              tabBarLabel: 'Profile',
              tabBarIcon: ({ color }) => (
                <FontAwesome5 name="user" color={color} size={22} />
              ),
              tabBarButton: props => <NavBarButton {...props}/>,
            }}
          >
            { (props) => <ProfileScreen {...props} app={this} /> }
          </Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>
    )
  }
}

export default function App() {
  let [fontsLoaded] = useFonts({
    'Yellowtail': require('./app/assets/fonts/yellowtail/Yellowtail-Regular.ttf'),
    'Architects-Daughter': require('./app/assets/fonts/architects_daughter/ArchitectsDaughter-Regular.ttf')
  })

  if (!fontsLoaded) {
    return <LoadingScreen/>
  }

  return (
    <Navigation/>
  )
}

const styles = StyleSheet.create({
  navigationBar: {
    backgroundColor: colors.main,
  },
})
