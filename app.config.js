let dailyManagerHost
if(process.env.ENV == 'production') {
  dailyManagerHost = 'https://daily-manager.mateuszdrewniak.it'
} else {
  dailyManagerHost = process.env.API_HOST || 'http://2d2faeaa5554.ngrok.io'
}

const settings = {
  expo: {
    name: "Daily Manager",
    slug: "daily-manager-react-native",
    version: "1.0.0",
    orientation: "portrait",
    icon: "./app/assets/icon.png",
    splash: {
      image: "./app/assets/splash.png",
      resizeMode: "contain",
      backgroundColor: "#ffffff"
    },
    updates: {
      fallbackToCacheTimeout: 0
    },
    assetBundlePatterns: [
      "**/*"
    ],
    ios: {
      supportsTablet: true
    },
    android: {
      adaptiveIcon: {
        foregroundImage: "./app/assets/adaptive-icon.png",
        backgroundColor: "#FFFFFF"
      }
    },
    web: {
      favicon: "./app/assets/favicon.png"
    },
    extra: {
      dailyManagerHost: dailyManagerHost
    },
  }
}

export default settings
